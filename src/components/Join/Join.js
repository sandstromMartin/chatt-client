import React,{useState} from 'react';
import { Link } from 'react-router-dom';

import './Join.css';

const Join = () => {
    const [name, setName] = useState('');
    const [room, setRoom] = useState('');    


    
//      const {token} = sessionStorage;
//  const socket = io.connect('http://localhost:5000');
//  socket.on('connect', function (socket) {
//    socket
//      .on('authenticated', function () {
//        //do other things
//      })
//      .emit('authenticate', {token}); //send the jwt
//  });
//  console.log(token)

    return (
        <div className="joinOuterContainer">
            <div className="joinInnerContainer">
                <h1 className="heading">Join</h1>
                <div><input placeholder="Name" className="joinInput" typ="text" onChange={(event) => setName(event.target.value)}/></div>
                <div><input placeholder="Room" className="joinInput mt-20" typ="text" onChange={(event) => setRoom(event.target.value)}/></div>
                <Link onClick={event => (!name || !room) ? event.preventDefault() : null} to={`/chat?name=${name}&room=${room}`}>
                <button className="button mt-20" type="submit" >Sign in</button>
                
                </Link>
            </div>
        </div>
    );
}
// const tokenFromAPI = fetch("http://localhost:5000/getToken", {
//             headers: {
//                 username: name
//             }
//         })
//             .then((resp) => resp.json())
//             .then((resp) => {
//                 return resp
//             })

export default Join;