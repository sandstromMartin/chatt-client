import React, { useState, useEffect }from 'react';
import queryString from 'query-string';
import io from 'socket.io-client';

import './Chat.css';

import InfoBar from '../Infobar/InfoBar';
import Input from '../Input/Input';
import Messages from '../Messages/Messages';
import UserList from '../UserList/UserList';
const endpoint = 'localhost:5000';
let socket = io(endpoint)

const Chat = ({location}) => {

    const [name, setName] = useState('');
    const [room, setRoom] = useState('');
    const [message, setMessage] = useState('');
    const [messages, setMessages] = useState([]);
    const [users, setUsers] = useState('');

    socket.emit('username', name);
   
    //retreve token and store it in session
    socket.on('token', token => {
        sessionStorage.setItem('NewToken', token);

        console.log(token)
    })
    useEffect(() => {

      
        //fetching data
        const {name, room} = queryString.parse(location.search)

        socket = io(endpoint);
       
        setName(name);
        setRoom(room);

        //console.log(socket)

        //socket method
        socket.emit('join', {name, room}, (error) =>{
            if(error) {
               console.log("upptaget namn")
            }
        });

        
    }, [ location.search]);

    useEffect(() => {
        //stores all messages into an array
        socket.on('message', message => {
            setMessages(messages => [ ...messages, message ]);
          });

          socket.on("roomData", ({users}) => {
              setUsers(users);
          })

    }, []);
    //function for sending messages
    const sendMessage = (event) => {
       
       event.preventDefault();

        if(message) {
            socket.emit('sendMessage', message, () => setMessage(''));
        }
        
    }
    console.log( message, messages);   
    
    

    return (
        <div className="outerContainer">
            <div className="container">
                <InfoBar room={room}/>
                <Messages messages={messages} name={name} />
                <Input message={message} setMessage={setMessage} sendMessage={sendMessage} /> 
            </div>
            <UserList users={users} />
        </div>
    );
}

export default Chat;