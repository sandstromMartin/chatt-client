#The server of the chat application
to start the server, open terminal and use the command "npm start".
On the client side, we start the react app with also "npm start. 

A multi-user chat application.
Used React, socket.io and JWT for authentication. 

First step to pick a username and join a chat room. If the name of the room is not existing, it will be created.

Next step is to login as another user in another incognito window (ctrl+shift+n) and join the same room. 


